import os

class MissingVariableException(Exception):
    pass

class EnvironmentVariables:
    def __init__(self):
        pass

    def get(self, name, defaultValue=None):
        if name in os.environ:
            return os.environ[name]
        elif defaultValue is None:
            print('[Error] Missing environment variable: ' + name)
            print('[Error] Please set that variable to a relevant value in the system and restart the pipeline.')
            exit(1)
        else:
            return defaultValue