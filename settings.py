from commandLineOptions import CommandLineOptions
from environmentVariables import EnvironmentVariables

class Settings:
    def __init__(self, commandLineOptions: CommandLineOptions, environmentVariables: EnvironmentVariables):
        self.commandLineOptions = commandLineOptions
        self.environmentVariables = environmentVariables
        self.data = dict()
        self.initSetting('projectPath', 'CI_PROJECT_DIR')
        self.initSetting('buildDirectory', 'UGB_BUILD_DIRECTORY')
        self.initSetting('projectName', 'CI_PROJECT_NAME')
        self.initSetting('platform', 'UGB_DEFAULT_PLATFORM')
        self.initSetting('incrementVersionString', 'UGB_ALWAYS_INCREMENT_VERSION_STRING', False)
        self.initSetting('pushTags', 'UGB_ALWAYS_PUSH_TAGS', False)
        self.initSetting('gitPullFirst', 'UGB_ALWAYS_RUN_GIT_PULL_FIRST', False)
        self.initSetting('gitRemote', 'UGB_GIT_REMOTE_OVERRIDE', 'origin')
        self.initSetting('gitBranch', 'UGB_GIT_BRANCH_OVERRIDE', 'master')

    def initSetting(self, settingName, envVarName, defaultValue=None):
        settingValue = self.commandLineOptions.get(settingName)
        if settingValue is None:
            settingValue = self.environmentVariables.get(envVarName, defaultValue)
        if settingValue is None:
            settingValue = defaultValue
        self.data[settingName] = settingValue

    def get(self, name):
        return self.data[name]