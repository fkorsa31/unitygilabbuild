from uploader import Uploader
from distutils import dir_util
import os
import shutil


class FirebaseUploader(Uploader):
    def __init__(self, environmentVariables, config, commandRunner):
        super(FirebaseUploader, self).__init__(environmentVariables, config)
        self.commandRunner = commandRunner
        self.firebasePath = environmentVariables.get('UGB_FIREBASE_PATH', None)
        self.firebaseProjectPath = environmentVariables.get('UGB_FIREBASE_PROJECT_PATH', None)

    def upload(self, appFilePath, releaseName):
        destinationPath = os.path.join(self.firebaseProjectPath, 'build')
        shutil.rmtree(destinationPath)
        if os.path.isfile(appFilePath):
            destinationFilePath = os.path.join(destinationPath, os.path.basename(appFilePath))
            os.remove(destinationFilePath)
            shutil.copy(appFilePath, destinationFilePath)
        else:
            dir_util.copy_tree(appFilePath, destinationPath)
        self.commandRunner.run([self.firebasePath, 'deploy'], self.firebaseProjectPath)
