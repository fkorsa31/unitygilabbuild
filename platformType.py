from enum import Enum

class PlatformType(Enum):
    Android = 1
    iOS = 2
    Windows = 3
    Mac = 4
    WebGL = 5