from git import Repo, Git
import re
import os

class GitHelper:
    def __init__(self, settings):
        self.settings = settings
        self.repo = Repo(settings.get('projectPath'))
        # Import the local machine's SSH configuration.
        gitEnvironment = Git()
        gitEnvironment.update_environment(
            **{k: os.environ[k] for k in os.environ if k.startswith('SSH')}
        )

    def pull(self):
        remoteName = self.settings.get('gitRemote')
        branchName = self.settings.get('gitBranch')
        remoteBranch = remoteName + '/' + branchName
        print('Resetting to ' + remoteBranch)
        self.repo.git.reset('--hard', remoteBranch)
        print('Fetching from ' + remoteName)
        self.repo.remotes[remoteName].fetch()
        print('Pulling ' + branchName + ' from ' + remoteName)
        self.repo.remotes[remoteName].pull(branchName)
        print('Resetting to ' + remoteBranch)
        self.repo.git.reset('--hard', remoteBranch)
        try:
            print('Resetting submodules...')
            self.repo.git.submodule('foreach', 'git', 'reset', '--hard')
            print('Updating submodules...')
            for submodule in self.repo.submodules:
                submodule.update(init=True)
            print('Successfully updated submodules!')
        except:
            print('Failed to update submodules! Continuing regardless...')
        print('Successfully pulled!')

    def getTagWithHighestIndex(self, prefix):
        highestBuildNumber = 1
        versionStringForHighestBuildNumber = '0.1.0'
        for tag in self.repo.tags:
            pattern = prefix + '([^-]+)-' + '([0-9]+)'
            matches = re.match(pattern, tag.name)
            if matches is not None:
                versionString = matches.group(1)
                buildNumberString = matches.group(2)
                try:
                    buildNumber = int(buildNumberString)
                    if buildNumber > highestBuildNumber:
                        versionStringForHighestBuildNumber = versionString
                        highestBuildNumber = buildNumber
                except:
                    print('Failed parsing tag: internal error')
        return (versionStringForHighestBuildNumber, highestBuildNumber)

    def createAndPushTag(self, prefix, buildNumber, versionString):
        tagName = prefix + versionString + '-' + str(buildNumber)
        print('Creating tag ' + tagName + '...')
        newTag = self.repo.create_tag(tagName)
        if self.settings.get('pushTags'):
            print('Pushing tag ' + tagName + '...')
            self.repo.remotes.origin.push(newTag)

    def printCommitSHA(self):
        print('(git) HEAD is at: ' + self.repo.head.commit.hexsha)