import unittest
import os
from pipeline import Pipeline
from builder import Builder
from uploader import Uploader
from environmentVariables import EnvironmentVariables
from commandRunner import CommandRunner
from defaultConfig import Config
from commandLineOptions import CommandLineOptions

class FakeBuilder:
    def build(self, projectPath, outputPath):
        self.projectPath = projectPath
        self.outputPath = outputPath

class FakeUploader:
    def upload(self, outputPath, versionString):
        self.outputPath = outputPath
        self.versionString = versionString

class EmptyObject:
    pass

class FakeEnvironmentVariables:
    def __init__(self):
        self.data = dict()
        self.data['CI_PROJECT_DIR'] = 'CI_PROJECT_DIR'
        self.data['UGB_BUILD_DIRECTORY'] = 'UGB_BUILD_DIRECTORY'
        self.data['CI_PROJECT_NAME'] = 'CI_PROJECT_NAME'

    def get(self, name):
        return self.data[name]

class FakeFileSystemHelper:
    def emptyDirectory(self, name):
        self.name = name

class TestRunPipeline(unittest.TestCase):
    def testRunPipeline(self):
        commandLineOptions = EmptyObject()
        environmentVariables = FakeEnvironmentVariables()
        builder = FakeBuilder()
        uploader = FakeUploader()
        fileSystemHelper = FakeFileSystemHelper()
        commandLineOptions.projectPath = 'projectPath'
        commandLineOptions.projectName = 'projectName'
        commandLineOptions.platform = EmptyObject()
        commandLineOptions.platform.name = 'platformName'
        pipeline = Pipeline(builder, uploader, fileSystemHelper, commandLineOptions, environmentVariables)
        pipeline.run()
        self.assertEqual(builder.projectPath, 'projectPath')
        self.assertEqual(builder.outputPath, os.path.join('UGB_BUILD_DIRECTORY', 'projectName', 'build.aab'))
        self.assertEqual(fileSystemHelper.name, os.path.join('UGB_BUILD_DIRECTORY', 'projectName'))


if __name__ == '__main__':
    unittest.main()