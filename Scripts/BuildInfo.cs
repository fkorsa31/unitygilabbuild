#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[CreateAssetMenu(fileName = "BuildInfo.asset", menuName = "UnityGitlabBuild/Build Info")]
public class BuildInfo : ScriptableObject
{
	public string lastBuildTime;
	public string repositoryVersion;
	public string versionString;

#if UNITY_EDITOR

	private void OnEnable()
	{
		repositoryVersion = GetGitVersion();
	}

	[MenuItem("StoriesOfGreed/Pring Git Version")]
	public static string GetGitVersion()
	{
		try
		{
			bool isWindows = Application.platform == RuntimePlatform.WindowsEditor;
			// Get the short commit hash of the current branch.
			string cmdArguments = isWindows ? "/c git rev-parse HEAD" : "-c \"git rev-parse HEAD\"";

			string processName = isWindows ? "cmd" : "/bin/bash";
			System.Diagnostics.ProcessStartInfo procStartInfo = new (processName, cmdArguments)
			{
				// The following commands are needed to redirect the standard output.
				// This means that it will be redirected to the Process.StandardOutput StreamReader.
				WorkingDirectory = Application.dataPath,
				RedirectStandardOutput = true,
				UseShellExecute = false,

				// Do not create the black window.
				CreateNoWindow = true
			};

			// Now we create a process, assign its ProcessStartInfo and start it
			System.Diagnostics.Process proc = new()
			{
				StartInfo = procStartInfo
			};
			proc.Start();

			string gitVersion = proc.StandardOutput.ReadToEnd();
			// Get the output into a string
			return gitVersion;
		}
		catch
		{
			Debug.LogError("Unable to get git commit hash.");
			return "unable to get version";
		}
	}
#endif
}