from pipeline import Pipeline
from builder import Builder
from uploaderFactory import UploaderFactory
from environmentVariables import EnvironmentVariables
from commandRunner import CommandRunner
from configLoader import ConfigLoader
from commandLineOptions import CommandLineOptions
from fileSystemHelper import FileSystemHelper
from gitHelper import GitHelper
from settings import Settings

commandLineOptions = CommandLineOptions()
commandLineOptions.parse()
environmentVariables = EnvironmentVariables()
configLoader = ConfigLoader()
config = configLoader.load()
settings = Settings(commandLineOptions, environmentVariables)
gitHelper = GitHelper(settings)
commandRunner = CommandRunner()
builder = Builder(commandRunner, environmentVariables, commandLineOptions, config)
uploaderFactory = UploaderFactory(commandLineOptions, environmentVariables, config, commandRunner)
uploader = uploaderFactory.createUploader()
fileSystemHelper = FileSystemHelper()
pipeline = Pipeline(builder, uploader, gitHelper, fileSystemHelper, settings, commandLineOptions)
pipeline.run()
