import shutil
import os

class FileSystemHelper:
    def emptyDirectory(self, directoryName):
        shutil.rmtree(directoryName, ignore_errors=True)
        os.makedirs(directoryName, exist_ok=True)