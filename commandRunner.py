import subprocess
import sys


class CommandRunner:
    def __init__(self):
        pass

    def run(self, arguments, workingDirectory=None):
        arguments_string = ' '.join(arguments)
        print(f'Running "{arguments_string}"')
        completedProcess = subprocess.run(arguments, stdout=sys.stdout, stderr=sys.stderr, text=True,
                                          cwd=workingDirectory, shell=True)
        if completedProcess.returncode is not 0:
            print(f'Command "{arguments_string}" failed with exit code {str(completedProcess.returncode)}! See output above.')
            exit(completedProcess.returncode)
