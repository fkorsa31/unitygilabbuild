import sys
from firebaseUploader import FirebaseUploader
from environmentVariables import EnvironmentVariables
from configLoader import ConfigLoader
from commandRunner import CommandRunner

inputFile = sys.argv[1]
versionString = sys.argv[2]
environmentVariables = EnvironmentVariables()
configLoader = ConfigLoader()
config = configLoader.load()
commandRunner = CommandRunner()
uploader = FirebaseUploader(environmentVariables, config, commandRunner)
uploader.upload(inputFile, versionString)