from platformType import PlatformType
import os


class Builder:
    def __init__(self, commandRunner, environmentVariables, commandLineOptions, config):
        self.commandRunner = commandRunner
        self.platform = commandLineOptions.get('platform')
        self.unityPath = environmentVariables.get('UGB_UNITY_PATH')
        self.config = config

    def computeUnityExePath(self, projectPath):
        versionFilePath = os.path.join(projectPath, 'ProjectSettings', 'ProjectVersion.txt')
        file = open(versionFilePath, 'r')
        versionLine = file.readline()
        file.close()
        versionString = versionLine.replace('m_EditorVersion: ', '').strip()
        return os.path.join(self.unityPath, versionString, 'Editor', 'Unity.exe')

    def build(self, projectPath, outputPath, buildNumber, versionString):
        buildClassName = ''
        buildTarget = ''
        if self.platform == PlatformType.Android:
            buildClassName = 'AndroidBuild'
            buildTarget = 'Android'
        elif self.platform == PlatformType.WebGL:
            buildClassName = 'WebGLBuild'
            buildTarget = 'WebGL'
        else:
            print('No builder defined for platform ' + self.platform + ' - cannot continue')
            exit(1)
        unityExePath = self.computeUnityExePath(projectPath)
        command = [unityExePath, '-batchmode', '-nographics', '-projectPath', projectPath, '-buildTarget', buildTarget,
             '-executeMethod', buildClassName + '.CommandLineBuild', '-outputPath', outputPath, '-buildNumber',
             str(buildNumber), '-versionString', versionString]
        if self.config.testAssemblies:
            command += ['-testAssemblies', ':'.join(self.config.testAssemblies)]
        self.commandRunner.run(command)
