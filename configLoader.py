import defaultConfig
import os,sys,inspect

class ConfigLoader:
    def load(self):
        currentDirectory = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        previousDirectory = currentDirectory
        parentDirectory = os.path.dirname(currentDirectory)
        while previousDirectory != parentDirectory:
            sys.path.insert(0, parentDirectory)
            try:
                import config
                return config
            except:
                parentDirectory = os.path.dirname(parentDirectory)
        return defaultConfig