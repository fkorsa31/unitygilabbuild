import argparse
from platformType import PlatformType


class CommandLineOptions:
    def __init__(self):
        self.data = dict()
        self.data['platform'] = PlatformType.WebGL
        self.data['isInternal'] = False
        self.data['projectPath'] = None
        self.data['projectName'] = None
        self.data['gitPullFirst'] = False
        self.data['gitRemote'] = 'origin'
        self.data['gitBranch'] = 'master'
        self.data['onlyUpload'] = False

    def parse(self):
        parser = argparse.ArgumentParser(description='Runs a build/sign/upload pipeline.')
        parser.add_argument('platform', choices=['Android', 'iOS', 'Windows', 'Mac', 'WebGL'],
                            help='target platform for the build')
        parser.add_argument('--internal', dest='isInternal', action='store_true',
                            help='when uploading, select an internal location as destination')
        parser.add_argument('-project-path', dest='projectPath',
                            help='path to the root of the Unity project. If not present, is taken from the gitlab '
                                 'pipeline environment variables.')
        parser.add_argument('-project-name', dest='projectName',
                            help='machine-friendly project name (no special characters, no spaces). Used for '
                                 'intermediate build directories.')
        parser.add_argument('--git-pull-first', dest='gitPullFirst', action='store_true',
                            help='before doing anything else, run git pull')
        parser.add_argument('-git-remote', dest='gitRemote',
                            help='the git remote to use when doing git operations')
        parser.add_argument('-git-branch', dest='gitBranch',
                            help='the git branch to use when doing git operations with manual pipelines on the local machine')
        parser.add_argument('--only-upload', dest='onlyUpload', action='store_true',
                            help='do nothing else than upload. Requires an existing build artifact to be present on disk.')

        args = parser.parse_args()
        self.data['platform'] = PlatformType[args.platform]
        self.data['isInternal'] = args.isInternal
        self.data['projectPath'] = args.projectPath
        self.data['projectName'] = args.projectName
        self.data['gitPullFirst'] = args.gitPullFirst
        self.data['gitRemote'] = args.gitRemote
        self.data['gitBranch'] = args.gitBranch
        self.data['onlyUpload'] = args.onlyUpload

    def get(self, name):
        if not name in self.data:
            return None
        return self.data[name]
