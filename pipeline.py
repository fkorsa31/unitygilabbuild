import os
import re
from platformType import PlatformType
from builder import Builder
from gitHelper import GitHelper
from uploader import Uploader
from fileSystemHelper import FileSystemHelper
from settings import Settings
from commandLineOptions import CommandLineOptions

class Pipeline:
    def __init__(self, builder: Builder, uploader: Uploader, gitHelper: GitHelper, 
                 fileSystemHelper: FileSystemHelper, settings: Settings, commandLineOptions: CommandLineOptions):
        self.builder = builder
        self.uploader = uploader
        self.settings = settings
        self.fileSystemHelper = fileSystemHelper
        self.gitHelper = gitHelper
        self.projectPath = self.settings.get('projectPath')
        self.commandLineOptions = commandLineOptions
        self.onlyUpload = commandLineOptions.get('onlyUpload')
        buildDirectory = self.getBuildDirectory()
        if not self.onlyUpload:
            self.fileSystemHelper.emptyDirectory(buildDirectory)
        platform = commandLineOptions.get('platform')
        outputName = 'build'
        if platform == PlatformType.Android:
            outputName = 'build.aab'
        elif platform == PlatformType.WebGL:
            outputName = 'build'
        else:
            print('No output name defined for platform ' + platform + ' - cannot continue')
            exit(1)
        self.outputPath = os.path.join(buildDirectory, outputName)

    def getBuildDirectory(self):
        buildDirectoryRoot = self.settings.get('buildDirectory')
        projectName = self.settings.get('projectName')
        buildDirectory = os.path.join(buildDirectoryRoot, projectName)
        return buildDirectory

    def incrementVersionString(self, versionString):
        matches = re.match(r'([0-9]+)\.([0-9]+)\.([0-9]+)')
        if matches is not None:
            try:
                major = int(matches.group(1))
                minor = int(matches.group(2))
                patch = int(matches.group(3))
                patch = patch + 1
                versionString = str(major) + '.' + str(minor) + '.' + str(patch)
            except:
                pass
        return versionString

    def run(self):
        platform = self.settings.get('platform')
        print('Pipeline started for target platform: ' + platform.name)

        if self.settings.get('gitPullFirst'):
            self.gitHelper.pull()
        self.gitHelper.printCommitSHA()
        tagPrefix = platform.name.lower() + '-build-'
        (versionString, buildNumber) = self.gitHelper.getTagWithHighestIndex(tagPrefix)
        buildNumber = buildNumber + 1
        if self.settings.get('incrementVersionString'):
            versionString = self.incrementVersionString(versionString)
        if not self.onlyUpload:
            self.builder.build(self.projectPath, self.outputPath, buildNumber, versionString)
        self.uploader.upload(self.outputPath, versionString)
        self.gitHelper.createAndPushTag(tagPrefix, buildNumber, versionString)
        print(f'Pipeline successful! Version: {versionString}.{buildNumber}')
