from androidUploader import AndroidUploader
from firebaseUploader import FirebaseUploader
from platformType import PlatformType


class UploaderFactory:
    def __init__(self, commandLineOptions, environmentVariables, config, commandRunner):
        self.commandLineOptions = commandLineOptions
        self.environmentVariables = environmentVariables
        self.config = config
        self.commandRunner = commandRunner

    def createUploader(self):
        platform = self.commandLineOptions.get('platform')
        if platform == PlatformType.Android:
            return AndroidUploader(self.environmentVariables, self.config)
        elif platform == PlatformType.WebGL:
            return FirebaseUploader(self.environmentVariables, self.config, self.commandRunner)
        print('Error: No uploader defined for platform ' + platform)
        exit(1)