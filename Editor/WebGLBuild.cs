﻿using UnityEditor;
using UnityEngine;

public class WebGLBuild : AutomatedBuild
{
	protected WebGLBuild(bool exitOnFinish) : base(exitOnFinish)
	{
	}

	public static void CommandLineBuild()
	{
		var webGLBuild = new WebGLBuild(true);
		webGLBuild.Build().Forget();
	}

	[MenuItem("Build/WebGL")]
	public static void OnMenuItemClicked()
	{
		var webGLBuild = new WebGLBuild(false);
		webGLBuild.Build().Forget();
	}

	[MenuItem("Build/Configure WebGL")]
	public static void OnConfigureMenuItemClicked()
	{
		var webGLBuild = new WebGLBuild(false);
		webGLBuild.PreBuildStep();
	}

	protected override void PreBuildStep()
	{
		UnityEditor.PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Brotli;
		EditorUserBuildSettings.SetPlatformSettings(
			BuildPipeline.GetBuildTargetName(UnityEditor.BuildTarget.WebGL), "CodeOptimization", "size");
	}

	protected override void PostBuildStep()
	{
	}

	protected override BuildTarget BuildTarget()
	{
		return UnityEditor.BuildTarget.WebGL;
	}
}
