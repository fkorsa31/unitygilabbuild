﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.TestTools.TestRunner.Api;
using UnityEngine;

public static class TestUtils
{
	/// <summary>
	/// Runs all edit mode tests.
	/// </summary>
	[MenuItem("Tools/Test/Run Edit Mode Tests")]
	public static void RunEditModeTests()
	{
		RunTests(TestMode.EditMode).Forget();
	}

	/// <summary>
	/// Runs all play mode tests.
	/// </summary>
	[MenuItem("Tools/Test/Run Play Mode Tests")]
	public static void RunPlayModeTests()
	{
		RunTests(TestMode.PlayMode).Forget();
	}

	public static async Task<bool> RunTests(TestMode testModeToRun, List<string>? testAssemblies = null)
	{
		var testRunnerApi = ScriptableObject.CreateInstance<TestRunnerApi>();
		var filter = new Filter(){testMode = testModeToRun};
		if (testAssemblies != null && testAssemblies.Count > 0)
		{
			filter.assemblyNames = testAssemblies.Select(s => s).ToArray();
		}

		TestCallbacks callbacks = new();
		testRunnerApi.RegisterCallbacks(callbacks);
		var tcs =
			new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
		int testCount = 0;
		testRunnerApi.RetrieveTestList(testModeToRun, (testRoot) => {
			testCount = testRoot.TestCaseCount;
			tcs.SetResult(true);
		});
		await tcs.Task;
		Debug.Log("Test count: " + testCount);
		if (testCount == 0)
		{
			return false;
		}
		testRunnerApi.Execute(new ExecutionSettings(filter));
		await callbacks.taskCompletionSource.Task;
		if (callbacks.hasFailedTest)
		{
			Debug.LogError("Some tests have failed! See above.");
		}
		else
		{
			Debug.Log("All tests have succeeded.");
		}
		return callbacks.hasFailedTest;
	}
}

public class TestCallbacks : ICallbacks
{
	public bool hasFailedTest
	{
		get;
		private set;
	}
	= false;
	public TaskCompletionSource<bool> taskCompletionSource =
		new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);

	public void RunStarted(ITestAdaptor testsToRun)
	{
	}

	public void RunFinished(ITestResultAdaptor result)
	{
		hasFailedTest = result.FailCount > 0;
		taskCompletionSource.SetResult(true);
	}

	public void TestStarted(ITestAdaptor test)
	{
	}

	public void TestFinished(ITestResultAdaptor result)
	{
		if (result.FailCount > 0)
		{
			Debug.LogError("Test failed: " + result.Name + " Output: " + result.Output);
		}
	}
}