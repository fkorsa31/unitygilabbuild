﻿using UnityEditor;

public class AndroidBuild : AutomatedBuild
{
	protected AndroidBuild(bool exitOnFinish) : base(exitOnFinish)
	{
	}

	public static void CommandLineBuild()
	{
		var androidBuild = new AndroidBuild(true);
		androidBuild.Build().Forget();
	}

	[MenuItem("Build/Android")]
	public static void OnMenuItemClicked()
	{
		var androidBuild = new AndroidBuild(false);
		androidBuild.Build().Forget();
	}

	[MenuItem("Build/Configure Android")]
	public static void OnConfigureMenuItemClicked()
	{
		var androidBuild = new AndroidBuild(false);
		androidBuild.PreBuildStep();
	}

	protected override void PreBuildStep()
	{
		EditorUserBuildSettings.androidBuildType = AndroidBuildType.Release;
		EditorUserBuildSettings.buildAppBundle = true;
#if UNITY_2020_1_OR_NEWER
		UnityEditor.PlayerSettings.Android.minifyDebug = true;
		UnityEditor.PlayerSettings.Android.minifyRelease = true;
#endif
		UnityEditor.PlayerSettings.Android.keystorePass =
			GetEnvironmentVariable("UGB_ANDROID_KEYSTORE_PASS");
		UnityEditor.PlayerSettings.Android.keyaliasPass =
			GetEnvironmentVariable("UGB_ANDROID_KEYALIAS_PASS");
		if (HasFlag("buildNumber"))
		{
			UnityEditor.PlayerSettings.Android.bundleVersionCode =
				int.Parse(GetFlagValue("buildNumber"));
		}
		else
		{
			UnityEditor.PlayerSettings.Android.bundleVersionCode =
				UnityEditor.PlayerSettings.Android.bundleVersionCode + 1;
		}

		UnityEditor.PlayerSettings.Android.targetArchitectures =
			AndroidArchitecture.ARM64 | AndroidArchitecture.ARMv7;

		UnityEditor.PlayerSettings.SetScriptingBackend(UnityEditor.Build.NamedBuildTarget.Android,
													   ScriptingImplementation.IL2CPP);
	}

	protected override void PostBuildStep()
	{
	}

	protected override BuildTarget BuildTarget()
	{
		return UnityEditor.BuildTarget.Android;
	}
}
