﻿using UnityEditor;
using UnityEngine;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

public abstract class AutomatedBuild
{
	private readonly bool exitOnFinish;

	protected AutomatedBuild(bool exitOnFinish)
	{
		this.exitOnFinish = exitOnFinish;
	}

	protected async Task Build()
	{
		EditorApplication.delayCall?.Invoke();
		if (!await RunTests())
		{
			FinishBuild(1);
			return;
		}
		UpdateBuildInfo();
		PreBuildStep();
		int exitCode = Compile();
		FinishBuild(exitCode);
	}

	private int Compile()
	{
		string outputPath = GetOutputPath();
		BuildPlayerOptions buildPlayerOptions = ParseOptions(outputPath);
		var buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);
		if (buildReport.summary.result != UnityEditor.Build.Reporting.BuildResult.Succeeded)
		{
			PrintCompilationErrors(buildReport);
			return 1;
		}

		PostBuildStep();
		Debug.Log("UGB: Bundle version is " + UnityEditor.PlayerSettings.bundleVersion);
		Debug.Log("Build succeeded! Output: " + outputPath);
		return 0;
	}

	private string GetOutputPath()
	{
		string outputPath = EditorUserBuildSettings.GetBuildLocation(BuildTarget());
		if (HasFlag("outputPath"))
		{
			outputPath = GetFlagValue("outputPath");
		}
		return outputPath;
	}

	private List<string> GetTestAssemblies()
	{
		List<string> testAssemblies = new ();
		if (HasFlag("testAssemblies"))
		{
			string testAssembliesString = GetFlagValue("testAssemblies");
			string[] tokens = testAssembliesString.Split(':');
			foreach (string token in tokens)
			{
				if (!string.IsNullOrEmpty(token))
				{
					testAssemblies.Add(token);
				}
			}
		}
		return testAssemblies;
	}

	private BuildPlayerOptions ParseOptions(string outputPath)
	{
		if (HasFlag("versionString"))
		{
			var bundleVersion = GetFlagValue("versionString");
			UnityEditor.PlayerSettings.bundleVersion = bundleVersion;
		}
		Debug.Log("Building for platform " + BuildTarget() + " to " + outputPath);
		var scenes = EditorBuildSettings.scenes;
		var sceneNames = new string[scenes.Length];
		for (int i = 0; i < scenes.Length; i++)
		{
			sceneNames[i] = scenes[i].path;
		}
		BuildPlayerOptions buildPlayerOptions =
			new (){scenes = sceneNames, target = BuildTarget(), locationPathName = outputPath};
		return buildPlayerOptions;
	}

	private static void PrintCompilationErrors(UnityEditor.Build.Reporting.BuildReport buildReport)
	{
		foreach (var step in buildReport.steps)
		{
			foreach (var message in step.messages)
			{
				if (message.type == LogType.Log)
				{
					Debug.Log(message.content);
				}
				else if (message.type == LogType.Error || message.type == LogType.Exception ||
						 message.type == LogType.Assert)
				{
					Debug.LogError(message.content);
				}
				else if (message.type == LogType.Warning)
				{
					Debug.LogWarning(message.content);
				}
			}
		}
	}

	// Returns true iff all tests have succeeded.
	private async Task<bool> RunTests()
	{
		List<string> testAssemblies = GetTestAssemblies();
		bool hasFailedEditModeTests = await TestUtils.RunTests(
			UnityEditor.TestTools.TestRunner.Api.TestMode.EditMode, testAssemblies);
		Debug.Log("Edit mode tests finished");
		bool hasFailedPlayModeTests = await TestUtils.RunTests(
			UnityEditor.TestTools.TestRunner.Api.TestMode.PlayMode, testAssemblies);
		Debug.Log("Play mode tests finished");
		return !hasFailedPlayModeTests && !hasFailedEditModeTests;
	}

	private void FinishBuild(int exitCode)
	{
		if (exitOnFinish)
		{
			Debug.Log("Building finished, exiting Unity...");
			EditorApplication.Exit(exitCode);
		}
	}

	protected string GetEnvironmentVariable(string name)
	{
		var value = System.Environment.GetEnvironmentVariable(name);
		if (value == null || value == "")
		{
			AbortBuild(
				"Environment variable " + name +
				" is not defined or empty. Please set it on the build machine and restart the script.");
			// Just to prevent the compiler warning about returning a possibly null value
			return "";
		}
		return value;
	}

	protected void AbortBuild(string errorMessage)
	{
		Debug.LogError(errorMessage);
		throw new System.Exception(errorMessage);
	}

	protected bool HasFlag(string flagName)
	{
		var commandLineArgs = System.Environment.GetCommandLineArgs();
		foreach (var arg in commandLineArgs)
		{
			if (arg == flagName || arg == "--" + flagName || arg == "-" + flagName)
			{
				return true;
			}
		}
		return false;
	}

	protected string GetFlagValue(string flagName, string? defaultValue = null)
	{
		var commandLineArgs = System.Environment.GetCommandLineArgs();
		bool foundFlag = false;
		foreach (var arg in commandLineArgs)
		{
			if (foundFlag)
			{
				return arg;
			}
			if (arg == flagName || arg == "--" + flagName || arg == "-" + flagName)
			{
				foundFlag = true;
			}
		}
		if (defaultValue != null)
		{
			return defaultValue;
		}
		AbortBuild("Missing value for flag " + flagName +
				   ". Please specify its value as an additional argument.");
		return "";
	}

	virtual protected void PreBuildStep()
	{
	}
	virtual protected void PostBuildStep()
	{
	}
	abstract protected BuildTarget BuildTarget();

	void UpdateBuildInfo()
	{
		string[] guids = AssetDatabase.FindAssets($"t:{typeof(BuildInfo)}");
		if (guids.Length > 1)
		{
			Debug.LogErrorFormat("Found more than 1 BuildInfo instances: {0}. Using first one!",
								 guids.Length);
		}

		if (guids.Length > 0)
		{
			string path = AssetDatabase.GUIDToAssetPath(guids[0]);
			BuildInfo buildInfo = AssetDatabase.LoadAssetAtPath<BuildInfo>(path);
			buildInfo.lastBuildTime = DateTime.Now.ToString("yyyy/MM/dd-HH:mm:ss");
			buildInfo.repositoryVersion = BuildInfo.GetGitVersion();
			string versionString = GetFlagValue("versionString", "unknown-version");
			string buildNumber = GetFlagValue("buildNumber", "unknown-build-number");
			buildInfo.versionString = versionString + "." + buildNumber;
			EditorUtility.SetDirty(buildInfo);
		}
		else
		{
			// TODO: AUTO-CREATE ONE!
			Debug.LogWarning("Couldn't find BuildInfo instance, please create one!");
		}
	}
}
