from uploader import Uploader
from git import Repo
from distutils import dir_util
import os
import shutil


class GitUploader(Uploader):
    def __init__(self, environmentVariables, config):
        super(GitUploader, self).__init__(environmentVariables, config)
        self.repoPath = environmentVariables.get('UGB_UPLOAD_GIT_REPO_PATH', None)
        self.branchName = environmentVariables.get('UGB_UPLOAD_GIT_BRANCH_NAME', 'master')
        self.remoteName = environmentVariables.get('UGB_UPLOAD_GIT_REMOTE_NAME', 'origin')

    def upload(self, appFilePath, releaseName):
        self.appFilePath = appFilePath
        self.releaseName = releaseName
        if self.repoPath is None:
            print('Error: the path to the git repository for upload was not set - cannot continue.')
            exit(1)
        repo = Repo(self.repoPath)
        if repo.is_dirty():
            print('Error: the repository for git upload is dirty - cannot continue.')
            exit(1)
        if repo.active_branch.name != self.branchName:
            print(
                'Error: the active branch in the git upload repository is not ' + self.branchName + ' - cannot continue.')
            exit(1)
        if os.path.isfile(appFilePath):
            destinationPath = os.path.join(self.repoPath, os.path.basename(appFilePath))
            os.remove(destinationPath)
            shutil.copy(appFilePath, destinationPath)
        else:
            dir_util.copy_tree(appFilePath, self.repoPath)
        repo.git.add(update=True)
        repo.index.commit('Automatic upload of build ' + releaseName)
        repo.remotes[self.remoteName].push(self.branchName)
