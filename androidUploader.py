from googleapiclient import discovery
from google.oauth2 import service_account
import mimetypes
from uploader import Uploader
import socket

timeout_in_sec = 60 * 3  # 3 minutes timeout limit
socket.setdefaulttimeout(timeout_in_sec)

class AndroidUploader(Uploader):
    def __init__(self, environmentVariables, config):
        super(AndroidUploader, self).__init__(environmentVariables, config)
        self.privateKeyFilePath = environmentVariables.get('UGB_GOOGLE_SERVICE_ACCOUNT_PRIVATE_KEY_FILE_PATH')
        self.packageName = config.androidPackageName
        self.track = config.androidTrack
        self.isDraft = config.androidAppIsDraft

        mimetypes.add_type("application/octet-stream", ".apk")
        mimetypes.add_type("application/octet-stream", ".aab")

    def upload(self, appFilePath, releaseName):
        self.appFilePath = appFilePath
        self.releaseName = releaseName
        service = self.authenticate()
        editId = self.createEdit(service)
        uploadResponse = self.uploadFile(self.appFilePath, editId, service)
        self.updateTrack(editId, service, uploadResponse)
        self.commitRequest(editId, service)
        print('Upload successful')

    def commitRequest(self, editId, service):
        print('Committing request...')
        service.edits().commit(
            editId=editId, packageName=self.packageName).execute()

    def updateTrack(self, editId, service, uploadResponse):
        print('Updating track "' + self.track + '" ...')
        service.edits().tracks().update(
            editId=editId,
            track=self.track,
            packageName=self.packageName,
            body={
                u'track': self.track,
                u'releases': [{
                    u'name': self.releaseName,
                    u'versionCodes': [str(uploadResponse['versionCode'])],
                    u'status': u'draft' if self.isDraft else u'completed',
                }]}).execute()

    def uploadFile(self, appFilePath, editId, service):
        print('Uploading ' + appFilePath + ' ...')
        if appFilePath.endswith('.aab'):
            endpoint = service.edits().bundles()
        else:
            endpoint = service.edits().apks()
        print('editId=' + editId + " packageName=" + self.packageName + " media_body=" + appFilePath)
        uploadResponse = endpoint.upload(
            editId=editId,
            packageName=self.packageName,
            media_body=appFilePath).execute()
        return uploadResponse

    def createEdit(self, service):
        editRequest = service.edits().insert(body={}, packageName=self.packageName)
        result = editRequest.execute()
        editId = result['id']
        return editId

    def authenticate(self):
        credentials = service_account.Credentials.from_service_account_file(
            self.privateKeyFilePath, scopes=['https://www.googleapis.com/auth/androidpublisher'])
        service = discovery.build('androidpublisher', 'v3', credentials=credentials)
        return service
