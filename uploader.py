class Uploader:
    def __init__(self, environmentVariables, config):
        self.environmentVariables = environmentVariables
        self.config = config

    def upload(self, appFilePath, releaseName):
        print('Failed to upload - did not override upload method in Uploader subclass!')
